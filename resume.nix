{ nixpkgs ? <nixpkgs> }: with import nixpkgs {};

stdenv.mkDerivation {
  name = "resume";
  src = ./.;

  buildInputs = [
    texlive.combined.scheme-small
  ];

  buildPhase = ''
    xelatex -file-line-error -interaction=nonstopmode resume.tex
  '';

  installPhase = ''
    cp resume.pdf $out
  '';
}
